window.onload = function () {

    //Consumindo API categorias
    let ENDPOINT_CATEGORIES = '/api/V1/categories/list'
    let RESPONSE_ENDPOINT_CATEGORIES = new XMLHttpRequest();

    RESPONSE_ENDPOINT_CATEGORIES.open('GET', ENDPOINT_CATEGORIES);
    RESPONSE_ENDPOINT_CATEGORIES.responseType = 'json';
    RESPONSE_ENDPOINT_CATEGORIES.send();

    RESPONSE_ENDPOINT_CATEGORIES.onload = function () {
        let resp = RESPONSE_ENDPOINT_CATEGORIES.response;
        loadCategories(resp)
    }


    //Evento menu Mobile
    if(window.innerWidth <= 800){
        $('#js--open-menu, #js--close-menu').on('click', function(){
            $("#js--menu-mobile").toggle("slow");
        });   

        $('#js--open-search').on('click', function(){
            $('#js--search-mobile').toggle('slow');
        });
    }
}


//Inserindo categorias e filtros
function loadCategories(response) {
    let mainList = document.getElementById('wj-categories-menu');
    let mainListMobile = document.getElementById('wj-categories-menu-mobile');
    let filterCategories = document.getElementById('js--filter-categories');
    let returnCategories = response['items'];


    for (let iterator = 0; iterator < returnCategories.length; iterator++) {
        let list = document.createElement('li');
        let listFilter = document.createElement('li');
        let ListMobile = document.createElement('li');

        list.className = "wj-nav-menu__item-sub js--nav-menu";
        list.setAttribute('data-id', returnCategories[iterator]['id']);
        mainList.appendChild(list);
        list.innerHTML = returnCategories[iterator]['name'];

        listFilter.className = "wj-section-category__filter-categories-option-item js--nav-menu";
        listFilter.setAttribute('data-id', returnCategories[iterator]['id']);
        filterCategories.appendChild(listFilter);
        listFilter.innerHTML = returnCategories[iterator]['name'];

        ListMobile.className = "wj-nav-menu-mobile__item-sub js--nav-menu";
        ListMobile.setAttribute('data-id', returnCategories[iterator]['id']);
        mainListMobile.appendChild(ListMobile);
        ListMobile.innerHTML = returnCategories[iterator]['name'];
    }

    listernerEventCategories();
}

//Observando eventos de clique nas categorias
function listernerEventCategories() {
    let categoriesListener = document.getElementsByClassName("js--nav-menu");
    let pageLoaded = document.getElementsByClassName("wj-nav-menu__item-sub")[2];

    for (let iterator = 0; iterator < categoriesListener.length; iterator++) {
        categoriesListener[iterator].addEventListener('click', function (e) {
            let callCategoryId = e.target.getAttribute('data-id');
            let callCategoryName = e.target.textContent;
            productDataReturn(callCategoryId, callCategoryName);
        });
    }

    pageLoaded.click();

}


//Consumindo API de produtos (utilizado Arrow Function)
let productDataReturn = (data, element) => {
    let ENDPOINT_PRODUCTS = '/api/V1/categories/'.concat(data);
    let RESPONSE_ENDPOINT_PRODUCTS = new XMLHttpRequest();

    RESPONSE_ENDPOINT_PRODUCTS.open('GET', ENDPOINT_PRODUCTS);
    RESPONSE_ENDPOINT_PRODUCTS.responseType = 'json';
    RESPONSE_ENDPOINT_PRODUCTS.send();

    RESPONSE_ENDPOINT_PRODUCTS.onload = function () {
        let resp = RESPONSE_ENDPOINT_PRODUCTS.response;
        loadProducts(resp, element);
    }
}

//Carregando Lista de Produtos e filtros
let loadProducts = (response, element) => {
    let listProducts = response['items'];
    let typeFilter = Object.values(response.filters[0]);

    let $productNodeDom = $('#js--category-products');
    let $filterNodeDom = $('#js--filter-dynamics');
    let $filterTitle = $('#js--filter-title');
    

    $filterTitle.empty()
    $productNodeDom.empty();
    $filterNodeDom.empty();

    $filterTitle.append(typeFilter);

    for (let iterator = 0; iterator < listProducts.length; iterator++) {
        let currentProductImage = listProducts[iterator]['image'];
        let currentProductName = listProducts[iterator]['name'];
        let currentProductPrice = (listProducts[iterator]['price']).toString();
        let currentSpecialPrice = (listProducts[iterator]['specialPrice']);
        let currentProductFilterImage = listProducts[iterator]['filter'][0]['thumb'];
        let currentProductFilter = Object.values(listProducts[iterator]['filter'][0]);
        let currentProductSku = Object.values(response['items'][iterator].filter[0])[0];

        if(currentSpecialPrice){
            $productNodeDom.append(
                `<div class="wj-section-category__products-items" data-sku="${currentProductSku}"> 
                    <img class="wj-section-category__products-image" src='${currentProductImage}' /> 
                    <div class="wj-section-category__products-name">${currentProductName}</div>
                    <div class="wj-section-category__products-offer">
                        <div class="wj-section-category__products-price wj-special-price">R$${currentSpecialPrice.toString().replace(".",",")}</div>
                        <div class="wj-section-category__products-price wj-total-price">R$${currentProductPrice.replace(".",",")}</div>
                    </div> 
                    <div class="wj-section-category__products-buy" id="js--action-buy">Comprar</div> 
                </div>`
            );
        }
        else{
            $productNodeDom.append(
                `<div class="wj-section-category__products-items" data-sku="${currentProductSku}"> 
                    <img class="wj-section-category__products-image" src='${currentProductImage}' /> 
                    <div class="wj-section-category__products-name">${currentProductName}</div>
                    <div class="wj-section-category__products-offer">
                        <div class="wj-section-category__products-price">R$${currentProductPrice.replace(".",",")}</div>
                    </div> 
                    <div class="wj-section-category__products-buy" id="js--action-buy">Comprar</div> 
                </div>`
            );
        }

        if (!currentProductFilterImage) {
            $filterNodeDom.append(
                `<li class="wj-section-category__filter-dynamic-item" data-filter="${currentProductFilter}">${currentProductFilter}</li>`
            );

            $filterNodeDom.removeClass("img-element");
        }
        else {
            $filterNodeDom.append(
                `<li class="wj-section-category__filter-dynamic-item" data-filter="${currentProductFilter}">${currentProductFilterImage}</li>`
            );

            $filterNodeDom.addClass("img-element");
        }
    }
    removeDuplicatesFilters();
    filterActionProduct();
    viewGridProduct();
    // loadSpecialFilters();
    fixProductPrice();
    updateData(element);
}

//Atualização do Breadcrumb
let updateData = (element) => {
    let $breadCrumbContent = $("#js--breadcrumb");
    let $categoryType = $('#js--category-name');
    $breadCrumbContent.html(element);
    $categoryType.html(element);
}

//Remover duplicatas filtro e ajustá-los
let removeDuplicatesFilters = () => {
    let $actualListFilter = Array.from($('.wj-section-category__filter-dynamic-item'));
    let $fixElementsFilter = $('#js--filter-dynamics');
    let arrayElement = [];

    $actualListFilter.forEach(function (element, index) {
        arrayElement.push(element.textContent);
    })
    arrayElement = arrayElement.filter((previusElement, nextElement) => arrayElement.indexOf(previusElement) === nextElement).sort();
    $fixElementsFilter.empty();

    arrayElement.forEach(function (element, index) {
        let skuProduct = arrayElement[index].replace("global/", "").replace(".png", "");
        if (arrayElement[index].includes("global/")) {

            $fixElementsFilter.append(`
                    <li class="wj-section-category__filter-dynamic-item-img js--filter-element">
                        <img src="${arrayElement[index]}" alt="cor ${skuProduct}" title="${skuProduct}" data-sku="${skuProduct}">
                    </li>
                `);
        }
        else {
            $fixElementsFilter.append(`
            <li class="wj-section-category__filter-dynamic-item js--filter-element" data-sku="${skuProduct}">${arrayElement[index]}</li>
        `);
        }
    });
    return true;
}

//Aplicando filtros
function filterActionProduct() {
    let $filterOption = Array.from($('.js--filter-element'));

    $filterOption.forEach(function (element, index) {
        element.addEventListener("click", function (e) {
            filterProducts(e.target.getAttribute('data-sku'));
        });

    });
    return true;
}

//Aplicando opções de grid
function viewGridProduct() {
    let $gridOption = $('#js-option-grid');
    let $rowOption = $('#js-option-row');
    let $listProducts = $('#js--category-products');

    $gridOption.on('click', function () {
        $listProducts.addClass('gridOption');
        $listProducts.removeClass('rowOption');
    });
    $rowOption.on('click', function () {
        $listProducts.addClass('rowOption');
        $listProducts.removeClass('gridOption');
    });

    return true;
}

//Aplicando filtros específicos
function filterProducts(currentFilter) {
    let $filterList = Array.from($('.wj-section-category__products-items'));

    for (let iterator = 0; iterator < $filterList.length; iterator++) {
        if ($filterList[iterator].getAttribute('data-sku').trim().toLowerCase() !== currentFilter.trim().toLowerCase()) {
            $filterList[iterator].style.display = "none";
        }
        else {
            $filterList[iterator].style.display = "flex";
        }
    }
}

//Correção de preços
function fixProductPrice() {
    let $productPrice = Array.from($('.wj-section-category__products-price'));

    $productPrice.forEach(function (element, index) {
        let arrayPrice = element.textContent.split(",");
        if (arrayPrice.length < 2) {
            let newPriceFirst = element.textContent.concat(",00");
            element.innerText = newPriceFirst;
        }
        else if(arrayPrice[arrayPrice.length-1]<10){
            let newPriceSecond = element.textContent.concat("0");
            element.innerText = newPriceSecond;
        }
        
    });

    return true;

}

